FROM traefik:v2.2.8

COPY traefik.yml /etc/traefik/.traefik.yml
COPY configuration.yml /etc/traefik/.configuration.yml

ARG TRAEFIK_API_HOST
ARG TRAEFIK_API_BASICAUTH_USER
ARG TRAEFIK_LETSENCRYPT_EMAIL

RUN apk add --no-cache gettext && \
    envsubst < /etc/traefik/.traefik.yml > /etc/traefik/traefik.yml && \
    envsubst < /etc/traefik/.configuration.yml > /etc/traefik/configuration.yml && \
    rm /etc/traefik/.traefik.yml /etc/traefik/.configuration.yml && \
    apk del gettext
